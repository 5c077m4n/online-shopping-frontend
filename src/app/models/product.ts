export interface Product {
	_id: string;
	name: string;
	categoryID: string;
	price: number;
	image: string;
	createdAt: Date;
	modifiedAt: Date;
}
