export interface Order {
	_id: string;
	customerId?: string;
	cartId?: string;
	finalPrice?: number;
	address?: {
		city?: string;
		street?: string;
		buildingNumber?: number;
		aptNumber?: number;
	};
	orderDate?: Date;
	creditCardNumber?: number;
	createdAt?: Date;
	modifiedAt?: Date;
}
