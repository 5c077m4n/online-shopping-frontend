export interface CartItem {
	_id?: string;
	productID?: string;
	cartID?: string;
	amount?: number;
	createdAt?: Date;
	modifiedAt?: Date;
}
