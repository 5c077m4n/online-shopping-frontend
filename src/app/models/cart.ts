export interface Cart {
	_id: string;
	customerID?: string;
	createdAt?: Date;
	modifiedAt?: Date;
}
