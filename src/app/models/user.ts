export interface User {
	_id: string;
	firstName?: string;
	lastName?: string;
	username?: string;
	email?: string;
	address?: {
		city?: string;
		street?: string;
		buildingNumber?: number;
		aptNumber?: number;
	};
	isManager?: boolean;
	secret?: string;
	password?: string;
	createdAt?: Date;
	modifiedAt?: Date;
	exp: number;
}
