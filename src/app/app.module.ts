import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FlexLayoutModule } from '@angular/flex-layout';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavBarComponent } from './components/nav-bar/nav-bar.component';
import { MaterialModule } from './modules/material/material.module';
import { httpInterceptorProviders } from './services/interceptors';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { ProductSearchComponent } from './components/product/product-search/product-search.component';
import {
	ProductComponent, CustomerBottomSheetComponent, ManagerBottomSheetComponent
} from './components/product/product-item/product.component';
import { ProductListComponent } from './components/product/product-list/product-list.component';
import { ProductTabsComponent } from './components/product/product-tabs/product-tabs.component';
import { ImageUploadComponent } from './components/image-upload/image-upload.component';
import { CartComponent, CartBottomSheetComponent } from './components/cart/cart.component';
import { WelcomePageComponent } from './components/welcome-page/welcome-page.component';


@NgModule({
	declarations: [
		AppComponent,
		NavBarComponent,
		LoginComponent,
		RegisterComponent,
		ProductSearchComponent,
		ProductComponent,
		CustomerBottomSheetComponent,
		ManagerBottomSheetComponent,
		ProductListComponent,
		ProductTabsComponent,
		ImageUploadComponent,
		CartComponent,
		CartBottomSheetComponent,
		WelcomePageComponent,
	],
	imports: [
		BrowserModule,
		AppRoutingModule,
		FormsModule,
		ReactiveFormsModule,
		HttpClientModule,
		FlexLayoutModule,
		MaterialModule
	],
	entryComponents: [
		CustomerBottomSheetComponent,
		ManagerBottomSheetComponent,
		CartBottomSheetComponent,
	],
	providers: [httpInterceptorProviders],
	bootstrap: [AppComponent]
})
export class AppModule {}
