import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule } from '@angular/material/button';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatMenuModule } from '@angular/material/menu';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatInputModule } from '@angular/material/input';
import { MatCardModule } from '@angular/material/card';
import { MatTabsModule } from '@angular/material/tabs';
import { MatBottomSheetModule } from '@angular/material/bottom-sheet';
import { MatListModule } from '@angular/material/list';
import { MatBadgeModule } from '@angular/material/badge';
import { MatIconModule } from '@angular/material/icon';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatDividerModule } from '@angular/material/divider';
import { MatExpansionModule } from '@angular/material/expansion';


const modules: any[] = [
	BrowserAnimationsModule,
	MatButtonModule,
	MatSnackBarModule,
	MatToolbarModule,
	MatMenuModule,
	MatSidenavModule,
	MatAutocompleteModule,
	MatInputModule,
	MatCardModule,
	MatTabsModule,
	MatBottomSheetModule,
	MatListModule,
	MatBadgeModule,
	MatIconModule,
	MatProgressSpinnerModule,
	MatDividerModule,
	MatExpansionModule,
];

@NgModule({
	imports: [
		CommonModule,
		...modules
	],
	exports: [
		...modules
	],
	entryComponents: [],
	declarations: []
})
export class MaterialModule {}
