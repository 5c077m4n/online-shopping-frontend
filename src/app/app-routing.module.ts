import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginGuard } from './guards/login.guard';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { ProductTabsComponent } from './components/product/product-tabs/product-tabs.component';
import { WelcomePageComponent } from './components/welcome-page/welcome-page.component';


const routes: Routes = [
	{ path: '', component: WelcomePageComponent, pathMatch: 'full' },
	{ path: 'login', component: LoginComponent, pathMatch: 'full' },
	{ path: 'register', component: RegisterComponent, pathMatch: 'full' },
	{ path: 'shop', component: ProductTabsComponent, pathMatch: 'full', canActivate: [LoginGuard] },
	{ path: '**', redirectTo: '', pathMatch: 'full' },
];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule]
})
export class AppRoutingModule {}
