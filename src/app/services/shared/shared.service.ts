import { Injectable } from '@angular/core';
import { Subject, Observable, Subscription } from 'rxjs';

import { SharedConstants } from './shared.enum';


@Injectable({ providedIn: 'root' }) export class SharedService {
	private subject = new Subject<any>();
	constructor() {}

	public emit(data: { type: SharedConstants, payload?: any }): void {
		this.subject.next(data);
    }
    public clear(): void {
        this.subject.next();
	}
	public getObservable(): Observable<any> {
		return this.subject.asObservable();
	}
    public subscribe(
		fn: (value: { type: SharedConstants, payload?: any }) => void
	): Subscription {
		return this.subject.asObservable()
			.subscribe(fn);
    }
}
