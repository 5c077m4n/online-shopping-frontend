import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { tap, catchError } from 'rxjs/operators';

import { LocalStorageService } from './local-storage.service';
import { User } from '../models/user';


@Injectable({ providedIn: 'root' }) export class AuthService {
	private readonly URL = '/api';
	constructor(
		private http: HttpClient,
		private storageService: LocalStorageService
	) {}

	public login(username: string, password: string): Observable<any> {
		return this.http.post<any>(
			`${this.URL}/login`, { username, password }
		)
			.pipe(
				tap(resp => {
					if(!resp.data) return console.error(
						`[LogService] There was an error in logging you in.`
					);
				}),
				catchError(this.handleError('login'))
			);
	}
	public register(user: User): Observable<any> {
		return this.http.post<any>(`${this.URL}/register`, user)
			.pipe(
				tap(resp => {
					if(!resp.data) console.error(
						`[LogService] There was an error in registering you.`
					);
				}),
				catchError(this.handleError('register'))
			);
	}
	public getRemotePublicKey(): Observable<any> {
		return this.http.get<any>(`${this.URL}/public-key`)
			.pipe(
				tap(res => {
					if(!res || !res.data || !res.data.publicKey) console.error(
						`[LogService] There was an error in getting the key.`
					);
					else this.storageService.add('publicKey', res.data.publicKey);
				}),
				catchError(this.handleError('register'))
			);
	}
	public getPublicKey(): string {
		return this.storageService.get('publicKey');
	}

	public getToken(): any {
		return this.storageService.get('rawToken');
	}
	public getCurrentUser(): User | undefined {
		const userData = this.storageService.get('currentUser');
		return (userData)? userData.user : undefined;
	}
	public isTokenExpired(): boolean {
		return (Date.now() > this.getCurrentUser().exp);
	}
	public isLoggedIn(): boolean {
		return (this.getCurrentUser() && !this.isTokenExpired());
	}
	public isManager(): boolean {
		return this.getCurrentUser().isManager;
	}

	public logout(): void {
		this.storageService.remove('rawToken');
		this.storageService.remove('currentUser');
	}

	/** @function handleError - Error handler */
	private handleError<T>(operation = 'operation', result?: T): (any) => Observable<T> {
		return (error: any): Observable<T> => {
			console.error(`[AuthService.${operation}()] Error: ${error.message}`);
			return of(result as T);
		};
	}
}
