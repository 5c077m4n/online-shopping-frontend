import { Injectable } from '@angular/core';

import { LocalStorageService } from './local-storage.service';
import { CartItem } from '../models/cart-item';


@Injectable({ providedIn: 'root' }) export class CartService {
	constructor(private storage: LocalStorageService) {}

	public async getCart(): Promise<CartItem> {
		return this.storage.get('cart') || {};
	}
	public async getItem(_id: string): Promise<CartItem> {
		let cart = this.storage.get('cart');
		if(!cart) return {} as CartItem;
		return cart[_id];
	}
	public async addItem(cartItem: CartItem): Promise<CartItem> {
		let cart = this.storage.get('cart') || {};
		this.storage.add('cart', Object.assign(cart, cartItem));
		return cart[cartItem._id];
	}
	public async updateItem(cartItem: CartItem): Promise<CartItem> {
		return this.addItem(cartItem);
	}
	public async removeItem(_id: string): Promise<void> {
		let cart = this.storage.get('cart') || {};
		cart[_id] = undefined;
		this.storage.add('cart', cart);
	}
	public async countItems(): Promise<number> {
		let cart = this.storage.get('cart') || {};
		return cart.length;
	}
	public async clearCart(): Promise<void> {
		return this.storage.remove('cart');
	}
}
