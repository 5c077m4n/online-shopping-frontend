import { TestBed, inject } from '@angular/core/testing';

import { SupermarketService } from './supermarket.service';

describe('SupermarketService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SupermarketService]
    });
  });

  it('should be created', inject([SupermarketService], (service: SupermarketService) => {
    expect(service).toBeTruthy();
  }));
});
