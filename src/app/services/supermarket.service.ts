import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { tap, catchError } from 'rxjs/operators';

import { User } from '../models/user';
import { Cart } from '../models/cart';
import { CartItem } from '../models/cart-item';
import { Product } from '../models/product';
import { Order } from '../models/order';
import { Category } from '../models/category';
import { AuthService } from './auth.service';


@Injectable({ providedIn: 'root' }) export class SupermarketService {
	private readonly URL = '/api';
	constructor(
		private http: HttpClient,
		private auth: AuthService
	) {}

	/** Customers */
	public getCustomers(): Observable<any> {
		return this.http.get<any>(`${this.URL}/customers`)
			.pipe(
				tap(resp => {
					if(!resp.data) return console.error(
						`[SupermarketService] There was an error in getting the customers.`
					);
				}),
				catchError(this.handleError('getCustomers'))
			);
	}
	public addCustomer(user: User): Observable<any> {
		return this.http.post<any>(`${this.URL}/customers`, user)
			.pipe(
				tap(resp => {
					if(!resp.data) return console.error(
						`[SupermarketService] There was an error in adding the customer.`
					);
				}),
				catchError(this.handleError('addCustomer'))
			);
	}

	public getCustomer(_id: string): Observable<any> {
		return this.http.get<any>(`${this.URL}/customers/${_id}`)
			.pipe(
				tap(resp => {
					if(!resp.data) return console.error(
						`[SupermarketService] There was an error in getting the customer.`
					);
				}),
				catchError(this.handleError('getCustomer'))
			);
	}
	public getCustomerCart(_id: string): Observable<any> {
		return this.http.get<any>(`${this.URL}/customers/${_id}/carts`)
			.pipe(
				tap(resp => {
					if(!resp) return console.error(
						`[SupermarketService] There was an error in getting the customer's cart.`
					);
				}),
				catchError(this.handleError('getCustomerCart'))
			);
	}
	public updateCustomer(user: User): Observable<any> {
		return this.http.put<any>(`${this.URL}/customers/${user._id}`, user)
			.pipe(
				tap(resp => {
					if(!resp.data) return console.error(
						`[SupermarketService] There was an error in editing the customer.`
					);
				}),
				catchError(this.handleError('updateCustomer'))
			);
	}
	public deleteCustomer(_id: string): Observable<any> {
		return this.http.delete<any>(
			`${this.URL}/customers/${_id}`,
			{ observe: 'response' }
		)
			.pipe(
				tap(resp => {
					if(resp.status !== 204) return console.error(
						`[SupermarketService] There was an error in deleting the customer.`
					);
				}),
				catchError(this.handleError('deleteCustomer'))
			);
	}

	/** Managers */
	public getManagers(): Observable<any> {
		return this.http.get<any>(`${this.URL}/managers`)
			.pipe(
				tap(resp => {
					if(!resp.data) return console.error(
						`[SupermarketService] There was an error in getting the manager list.`
					);
				}),
				catchError(this.handleError('getManagers'))
			);
	}
	public addManager(user: User): Observable<any> {
		return this.http.post<any>(`${this.URL}/managers`, user)
			.pipe(
				tap(resp => {
					if(!resp.data) return console.error(
						`[SupermarketService] There was an error in adding the manager.`
					);
				}),
				catchError(this.handleError('addManager'))
			);
	}

	public getManager(username: string): Observable<any> {
		return this.http.get<any>(`${this.URL}/managers/${username}`)
			.pipe(
				tap(resp => {
					if(!resp.data) return console.error(
						`[SupermarketService] There was an error in getting the manager.`
					);
				}),
				catchError(this.handleError('getManager'))
			);
	}
	public updateManager(user: User): Observable<any> {
		return this.http.put<any>(`${this.URL}/managers/${user.username}`, user)
			.pipe(
				tap(resp => {
					if(!resp.data) return console.error(
						`[SupermarketService] There was an error in editing the manager.`
					);
				}),
				catchError(this.handleError('updateManager'))
			);
	}
	public deleteManager(username: string): Observable<any> {
		return this.http.delete<any>(`${this.URL}/managers/${username}`, { observe: 'response' })
			.pipe(
				tap(resp => {
					if(resp.status !== 204) return console.error(
						`[SupermarketService] There was an error in deleting the manager.`
					);
				}),
				catchError(this.handleError('deleteManager'))
			);
	}

	/** Carts */
	public getCarts(): Observable<any> {
		const userID = this.auth.getCurrentUser()._id;
		return this.http.get<any>(`${this.URL}/customers/${userID}/carts`)
			.pipe(
				tap(resp => {
					if(!resp.data) return console.error(
						`[SupermarketService] There was an error in getting the cart list.`
					);
				}),
				catchError(this.handleError('getCarts'))
			);
	}
	public addCart(cart: Cart): Observable<any> {
		const userID = this.auth.getCurrentUser()._id;
		return this.http.post<any>(`${this.URL}/customers/${userID}/carts`, cart)
			.pipe(
				tap(resp => {
					if(!resp.data) return console.error(
						`[SupermarketService] There was an error in adding the cart.`
					);
				}),
				catchError(this.handleError('addCart'))
			);
	}
	public getCart(_id: string): Observable<any> {
		const userID = this.auth.getCurrentUser()._id;
		return this.http.get<any>(`${this.URL}/customers/${userID}/carts/${_id}`)
			.pipe(
				tap(resp => {
					if(!resp) return console.error(
						`[SupermarketService] There was an error in getting the cart.`
					);
				}),
				catchError(this.handleError('getCart'))
			);
	}
	public updateCart(cart: Cart): Observable<any> {
		const userID = this.auth.getCurrentUser()._id;
		return this.http.put<any>(`${this.URL}/customers/${userID}/carts/${cart._id}`, cart)
			.pipe(
				tap(resp => {
					if(!resp.data) return console.error(
						`[SupermarketService] There was an error in editing the cart.`
					);
				}),
				catchError(this.handleError('updateCart'))
			);
	}
	public deleteCart(_id: string): Observable<any> {
		const userID = this.auth.getCurrentUser()._id;
		return this.http.delete<any>(
			`${this.URL}/customers/${userID}/carts/${_id}`,
			{ observe: 'response' }
		)
			.pipe(
				tap(resp => {
					if(resp.status !== 204) return console.error(
						`[SupermarketService] There was an error in deleting the cart.`
					);
				}),
				catchError(this.handleError('deleteCart'))
			);
	}

	/** Cart's Items */
	public getCartItems(_id: string): Observable<any> {
		const userID = this.auth.getCurrentUser()._id;
		return this.http.get<any>(`${this.URL}/customers/${userID}/carts/${_id}/items`)
			.pipe(
				tap(resp => {
					if(!resp.data) return console.error(
						`[SupermarketService] There was an error in getting the cart's items.`
					);
				}),
				catchError(this.handleError('getCartItems'))
			);
	}
	public countCartItems(_id: string): Observable<any> {
		const userID = this.auth.getCurrentUser()._id;
		return this.http.get<any>(`${this.URL}/customers/${userID}/carts/${_id}/items/count`)
			.pipe(
				tap(resp => {
					if(!resp.data) return console.error(
						`[SupermarketService] There was an error in getting the cart's item count.`
					);
				}),
				catchError(this.handleError('countCartItems'))
			);
	}
	public addCartItem(cartItem: CartItem): Observable<any> {
		const userID = this.auth.getCurrentUser()._id;
		return this.http.post<any>(`${this.URL}/customers/${userID}/carts/${cartItem.cartID}/items`, cartItem)
			.pipe(
				tap(resp => {
					if(!resp.data) return console.error(
						`[SupermarketService] There was an error in getting the cart's items.`
					);
				}),
				catchError(this.handleError('addCartItem'))
			);
	}
	public getCartItem(cartID: string, itemID: string): Observable<any> {
		const userID = this.auth.getCurrentUser()._id;
		return this.http.get<any>(`${this.URL}/customers/${userID}/carts/${cartID}/items/${itemID}`)
			.pipe(
				tap(resp => {
					if(!resp.data) return console.error(
						`[SupermarketService] There was an error in getting the cart's item.`
					);
				}),
				catchError(this.handleError('getCartItem'))
			);
	}
	public deleteCartItem(cartID: string, itemID: string): Observable<any> {
		const userID = this.auth.getCurrentUser()._id;
		return this.http.delete<any>(
			`${this.URL}/customers/${userID}/carts/${cartID}/items/${itemID}`,
			{ observe: 'response' }
		)
			.pipe(
				tap(resp => {
					if(resp.status !== 204) return console.error(
						`[SupermarketService] There was an error in deleting the cart's item.`
					);
				}),
				catchError(this.handleError('deleteCartItem'))
			);
	}
	public getCartItemAsProduct(cartID: string, itemID: string): Observable<any> {
		const userID = this.auth.getCurrentUser()._id;
		return this.http.get<any>(
			`${this.URL}/customers/${userID}/carts/${cartID}/items/${itemID}/product`
		)
			.pipe(
				tap(resp => {
					if(!resp.data) return console.error(
						`[SupermarketService] There was an error in getting the cart's product.`
					);
				}),
				catchError(this.handleError('getCartItemAsProduct'))
			);
	}

	/** Products */
	public getProducts(): Observable<any> {
		return this.http.get<any>(`${this.URL}/products`)
			.pipe(
				tap(resp => {
					if(!resp.data) return console.error(
						`[SupermarketService] There was an error in getting the product list.`
					);
				}),
				catchError(this.handleError('getProducts'))
			);
	}
	public countProducts(): Observable<any> {
		return this.http.get<any>(`${this.URL}/statistics/count-products`)
			.pipe(
				tap(resp => {
					if(!resp.data) return console.error(
						`[SupermarketService] There was an error in getting the product count.`
					);
				}),
				catchError(this.handleError('countProducts'))
			);
	}
	public getProduct(_id: string): Observable<any> {
		return this.http.get<any>(`${this.URL}/products/${_id}`)
			.pipe(
				tap(resp => {
					if(!resp.data) return console.error(
						`[SupermarketService] There was an error in getting the product.`
					);
				}),
				catchError(this.handleError('getProduct'))
			);
	}
	public addProduct(product: Product): Observable<any> {
		return this.http.post<any>(`${this.URL}/products`, product)
			.pipe(
				tap(resp => {
					if(!resp.data) return console.error(
						`[SupermarketService] There was an error in adding the product.`
					);
				}),
				catchError(this.handleError('addProduct'))
			);
	}
	public updateProduct(product: Product): Observable<any> {
		return this.http.put<any>(`${this.URL}/products/${product._id}`, product)
			.pipe(
				tap(resp => {
					if(!resp.data) return console.error(
						`[SupermarketService] There was an error in editing the product.`
					);
				}),
				catchError(this.handleError('updateProduct'))
			);
	}
	public deleteProduct(_id: string): Observable<any> {
		return this.http.delete<any>(`${this.URL}/products/${_id}`, { observe: 'response' })
			.pipe(
				tap(resp => {
					if(resp.status !== 204) return console.error(
						`[SupermarketService] There was an error in deleting the product.`
					);
				}),
				catchError(this.handleError('deleteProduct'))
			);
	}

	public getProductCategory(_id: string): Observable<any> {
		return this.http.get<any>(`${this.URL}/products/${_id}/category`)
			.pipe(
				tap(resp => {
					if(!resp.data) return console.error(
						`[SupermarketService] There was an error in getting the product's category.`
					);
				}),
				catchError(this.handleError('getProductCategory'))
			);
	}
	public updateProductCategory(_id: string, categoryID: string): Observable<any> {
		return this.http.put<any>(`${this.URL}/products/${_id}/category`, { categoryID })
			.pipe(
				tap(resp => {
					if(!resp.data) return console.error(
						`[SupermarketService] There was an error in updating the product's category.`
					);
				}),
				catchError(this.handleError('updateProductCategory'))
			);
	}

	/** Categories */
	public getCategories(): Observable<any> {
		return this.http.get<any>(`${this.URL}/categories`)
			.pipe(
				tap(resp => {
					if(!resp.data) return console.error(
						`[SupermarketService] There was an error in getting the category list.`
					);
				}),
				catchError(this.handleError('getCategories'))
			);
	}
	public getCategory(_id: string): Observable<any> {
		return this.http.get<any>(`${this.URL}/categories/${_id}`)
			.pipe(
				tap(resp => {
					if(!resp.data) return console.error(
						`[SupermarketService] There was an error in getting the category.`
					);
				}),
				catchError(this.handleError('getCategory'))
			);
	}
	public addCategory(category: Category): Observable<any> {
		return this.http.post<any>(`${this.URL}/categories`, category)
			.pipe(
				tap(resp => {
					if(!resp.data) return console.error(
						`[SupermarketService] There was an error in adding the category.`
					);
				}),
				catchError(this.handleError('addCategory'))
			);
	}
	public updateCategory(category: Category): Observable<any> {
		return this.http.put<any>(`${this.URL}/categories/${category._id}`, category)
			.pipe(
				tap(resp => {
					if(!resp.data) return console.error(
						`[SupermarketService] There was an error in editing the category.`
					);
				}),
				catchError(this.handleError('updateCategory'))
			);
	}
	public deleteCategory(_id: string): Observable<any> {
		return this.http.delete<any>(`${this.URL}/categories/${_id}`, { observe: 'response' })
			.pipe(
				tap(resp => {
					if(resp.status !== 204) return console.error(
						`[SupermarketService] There was an error in deleting the category.`
					);
				}),
				catchError(this.handleError('deleteCategory'))
			);
	}

	public getCategoryProducts(_id: string) {
		return this.http.get<any>(`${this.URL}/categories/${_id}/products`)
			.pipe(
				tap(resp => {
					if(!resp.data) return console.error(
						`[SupermarketService] There was an error in getting the category's products.`
					);
				}),
				catchError(this.handleError('getCategoryProducts'))
			);
	}

	/** Orders */
	public getOrders(): Observable<any> {
		return this.http.get<any>(`${this.URL}/orders`)
			.pipe(
				tap(resp => {
					if(!resp.data) return console.error(
						`[SupermarketService] There was an error in getting the orders.`
					);
				}),
				catchError(this.handleError('getOrders'))
			);
	}
	public countOrders(): Observable<any> {
		return this.http.get<any>(`${this.URL}/statistics/count-products`)
			.pipe(
				tap(resp => {
					if(!resp.data) return console.error(
						`[SupermarketService] There was an error in getting the number of orders.`
					);
				}),
				catchError(this.handleError('countOrders'))
			);
	}
	public addOrder(order: Order): Observable<any> {
		return this.http.post<any>(`${this.URL}/orders`, order)
			.pipe(
				tap(resp => {
					if(!resp.data) return console.error(
						`[SupermarketService] There was an error in adding the order.`
					);
				}),
				catchError(this.handleError('addOrder'))
			);
	}

	public getOrder(_id: string): Observable<any> {
		return this.http.get<any>(`${this.URL}/orders/${_id}`)
			.pipe(
				tap(resp => {
					if(!resp.data) return console.error(
						`[SupermarketService] There was an error in getting the order.`
					);
				}),
				catchError(this.handleError('getOrder'))
			);
	}
	public updateOrder(order: Order): Observable<any> {
		return this.http.put<any>(`${this.URL}/orders/${order._id}`, order)
			.pipe(
				tap(resp => {
					if(!resp.data) return console.error(
						`[SupermarketService] There was an error in editing the order.`
					);
				}),
				catchError(this.handleError('updateOrder'))
			);
	}
	public deleteOrder(_id: string): Observable<any> {
		return this.http.delete<any>(`${this.URL}/orders/${_id}`, { observe: 'response' })
			.pipe(
				tap(resp => {
					if(resp.status !== 204) return console.error(
						`[SupermarketService] There was an error in deleting the order.`
					);
				}),
				catchError(this.handleError('deleteOrder'))
			);
	}

	/** @function handleError - Error handler */
	private handleError<T>(operation = 'operation', result?: T): (any) => Observable<T> {
		return (error: any): Observable<T> => {
			console.error(`[SupermarketService.${operation}()] Error: ${error.message}`);
			return of(result as T);
		};
	}
}
