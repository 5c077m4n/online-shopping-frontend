import { Injectable } from '@angular/core';
import { HttpClient, HttpEventType, HttpEvent, HttpHeaders, HttpRequest } from '@angular/common/http';
import { tap, catchError, map, last } from 'rxjs/operators';
import { Observable, of } from 'rxjs';


@Injectable({ providedIn: 'root' }) export class ImageUploadService {
	private readonly url = '/api/upload-image';
	constructor(private http: HttpClient) {}

	public uploadImage(imageFile: File) {
		const req = new HttpRequest('POST', this.url, imageFile, {
			reportProgress: true
		});
		return this.http.request(req).pipe(
			map(event => this.getEventMessage(event, imageFile)),
			tap(console.log),
			last(), // return last (completed) message to caller
			catchError(this.handleError('uploadImage'))
		);
	}

	/** Return distinct message for sent, upload progress, & response events */
	private getEventMessage(event: HttpEvent<any>, file: File) {
		switch(event.type) {
			case HttpEventType.Sent:
				return `Uploading file "${file.name}" of size ${file.size}.`;
			case HttpEventType.UploadProgress:
				const percentDone = Math.round(100 * event.loaded / event.total);
				return `File "${file.name}" is ${percentDone}% uploaded.`;
			case HttpEventType.Response:
				return `File "${file.name}" was completely uploaded!`;
			default:
				return `File "${file.name}" surprising upload event: ${event.type}.`;
		};
	}

	/** @function handleError - Error handler */
	private handleError<T>(operation = 'operation', result?: T): (any) => Observable<T> {
		return (error: any): Observable<T> => {
			console.error(`[ImageUploadService.${operation}()] Error: ${error.message}`);
			return of(result as T);
		};
	}
}
