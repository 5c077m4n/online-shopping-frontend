import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { tap, catchError } from 'rxjs/operators';


@Injectable({ providedIn: 'root' }) export class LogService {
	private readonly URL: string = '/api/log';
	constructor(private http: HttpClient) {}

	getAll(): Observable<any> {
		return this.http.get<string>(this.URL)
			.pipe(
				tap(logs => {
					if(!logs) console.error(
						`[LogService] There was an error in getting the logs.`
					);
				}),
				catchError(this.handleError('getAllLogs'))
			);
	}

	add(log: string): Observable<any> {
		return this.http.post<any>(this.URL, { log })
			.pipe(
				tap(resp => {
					if(!resp)
						console.error(`[LogService] There was an error in posting the logs.`);
				}),
				catchError(this.handleError('log'))
			);
	}

	clearAll(log: string): Observable<any> {
		return this.http.delete<HttpResponse<any>>(this.URL, {
			observe: 'response'
		})
			.pipe(
				tap(resp => (resp.status !== 204)?
					console.error(
						`[LogService] There was an error in deleting the logs.`
					) : undefined
				),
				catchError(this.handleError('clearLog'))
			);
	}

	/** @function handleError - Error handler */
	private handleError<T>(operation = 'operation', result?: T) {
		return (error: any): Observable<T> => {
			console.error(`[SchoolService.${operation}()] Error: ${error.message}`);
			return of(result as T);
		};
	}
}
