import { Injectable } from '@angular/core';


@Injectable({ providedIn: 'root' }) export class LocalStorageService {
	constructor() {}

	public add(key: string, value: any): void {
		return window.localStorage.setItem(
			key,
			JSON.stringify({ value })
		);
	}
	public get(key: string): any {
		const data = window.localStorage.getItem(key);
		return (data)? JSON.parse(data).value : undefined;
	}
	public remove(key: string): void {
		return window.localStorage.removeItem(key);
	}
	public clear(): void {
		return window.localStorage.clear();
	}
}
