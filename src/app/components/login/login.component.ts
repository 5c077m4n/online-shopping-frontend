import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';

import { AuthService } from '../../services/auth.service';
import { SnackBarService } from '../../services/snack-bar.service';


@Component({
	selector: 'app-login',
	changeDetection: ChangeDetectionStrategy.OnPush,
	templateUrl: './login.component.html',
	styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
	public loginControl: FormGroup;
	constructor(
		private fb: FormBuilder,
		private router: Router,
		private auth: AuthService,
		private snackBar: SnackBarService
	) {
		this.loginControl = this.fb.group({
			username: ['', Validators.required],
			password: ['', Validators.required]
		});
	}
	ngOnInit() {}

	onSubmit() {
		this.auth.login(
			this.loginControl.value.username,
			this.loginControl.value.password
		)
			.subscribe(data => {
				if(!data) return;
				this.snackBar.show('Good to see you back :D');
				this.router.navigate(['']);
			});
	}
}
