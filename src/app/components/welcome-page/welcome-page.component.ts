import { Component, OnInit } from '@angular/core';
import { map, tap } from 'rxjs/operators';
import { Observable } from 'rxjs';

import { AuthService } from '../../services/auth.service';
import { SupermarketService } from '../../services/supermarket.service';


@Component({
	selector: 'app-welcome-page',
	templateUrl: './welcome-page.component.html',
	styleUrls: ['./welcome-page.component.css']
})
export class WelcomePageComponent implements OnInit {
	public count: number;
	constructor(
		private auth: AuthService,
		private market: SupermarketService
	) {}
	ngOnInit() {
		this.countProduct$.subscribe();
	}

	public get countProduct$(): Observable<number> {
		return this.market.countProducts()
			.pipe(
				map(res => res && res.data || 0),
				tap(count => this.count = count),
			);
	}
	public get isLoggedIn(): boolean {
		return this.auth.isLoggedIn();
	}
}
