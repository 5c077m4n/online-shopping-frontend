import { Component, OnInit, Input, Inject, ChangeDetectionStrategy } from '@angular/core';
import { MatBottomSheet, MatBottomSheetRef, MAT_BOTTOM_SHEET_DATA } from '@angular/material';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { Product } from '../../../models/product';
import { AuthService } from '../../../services/auth.service';
import { SnackBarService } from '../../../services/snack-bar.service';
import { SupermarketService } from '../../../services/supermarket.service';
import { SharedService } from '../../../services/shared/shared.service';
import { SharedConstants } from '../../../services/shared/shared.enum';
import { CartItem } from '../../../models/cart-item';
import { LocalStorageService } from '../../../services/local-storage.service';


@Component({
	selector: 'app-product',
	templateUrl: './product.component.html',
	styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {
	@Input() public product: Product;
	public productControl: FormGroup;
	constructor(
		private bottomSheet: MatBottomSheet,
		private fb: FormBuilder,
		private auth: AuthService
	) {}
	ngOnInit() {
		this.productControl = this.fb.group({
			_id: [this.product._id, Validators.required],
			name: [this.product.name, Validators.required],
			price: [this.product.price, [Validators.required, Validators.pattern(/^\d+.?\d*$/g)]],
			amount: ['1', [Validators.required, Validators.pattern(/^\d+$/g)]],
		});
	}

	public isManager(): boolean {
		return this.auth.isManager();
	}
	public customerBottomSheet(): void {
		this.bottomSheet.open(
			CustomerBottomSheetComponent,
			{ data: this.productControl }
		);
	}
	public managerBottomSheet(): void {
		this.bottomSheet.open(
			ManagerBottomSheetComponent,
			{ data: this.productControl }
		);
	}
}

@Component({
	templateUrl: 'customer-bottom-sheet.component.html',
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CustomerBottomSheetComponent {
	private customerID: string = this.auth.getCurrentUser()._id;
	constructor(
		private bottomSheetRef: MatBottomSheetRef<CustomerBottomSheetComponent>,
		private snackBar: SnackBarService,
		private auth: AuthService,
		private market: SupermarketService,
		private messenger: SharedService,
		private storage: LocalStorageService,
		@Inject(MAT_BOTTOM_SHEET_DATA) public productControl: FormGroup
	) {}

	onSubmit(e: MouseEvent): void {
		e.preventDefault();
		this.market.addCartItem({
			customerID: this.customerID,
			cartID: this.storage.get('cartID'),
			productID: this.productControl.value._id,
			amount: this.productControl.value.amount
		} as CartItem)
			.subscribe(res => {
				if(!res) return;
				this.messenger.emit({ type: SharedConstants.REFRESH_CART });
				this.snackBar.show(`${this.productControl.value.name} has been added successfully.`);
				this.bottomSheetRef.dismiss();
			});
	}
}

@Component({
	templateUrl: 'manager-bottom-sheet.component.html',
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ManagerBottomSheetComponent {
	constructor(
		private bottomSheetRef: MatBottomSheetRef<ManagerBottomSheetComponent>,
		private snackBar: SnackBarService,
		private market: SupermarketService,
		private messenger: SharedService,
		@Inject(MAT_BOTTOM_SHEET_DATA) public productControl: FormGroup
	) {}

	onSubmit(event: MouseEvent): void {
		event.preventDefault();
		if(!this.productControl.valid) return;
		this.market.updateProduct(this.productControl.value)
			.subscribe(res => {
				if(!res) return;
				this.messenger.emit({ type: SharedConstants.REFRESH_PRODUCT_LIST });
				this.snackBar.show(`${this.productControl.value.name} has been edited successfully.`);
				this.bottomSheetRef.dismiss();
			});
	}

	public deleteProd(): void {
		this.market.deleteProduct(this.productControl.value._id)
			.subscribe(data => {
				if(!data) return;
				this.messenger.emit({ type: SharedConstants.REFRESH_PRODUCT_LIST });
				this.snackBar.show(`${this.productControl.value.name} has been deleted successfully.`);
				this.bottomSheetRef.dismiss();
			});
	}
}
