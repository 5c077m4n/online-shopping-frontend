import {
	Component, OnInit, OnDestroy, ChangeDetectionStrategy, ChangeDetectorRef
} from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Subscription, Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';

import { SharedConstants } from '../../../services/shared/shared.enum';
import { Category } from '../../../models/category';
import { SupermarketService } from '../../../services/supermarket.service';
import { AuthService } from '../../../services/auth.service';
import { SharedService } from '../../../services/shared/shared.service';
import { SnackBarService } from '../../../services/snack-bar.service';


@Component({
	selector: 'app-product-tabs',
	changeDetection: ChangeDetectionStrategy.OnPush,
	templateUrl: './product-tabs.component.html',
	styleUrls: ['./product-tabs.component.css']
})
export class ProductTabsComponent implements OnInit, OnDestroy {
	public categories: Category[];
	public newProductControl: FormGroup;
	private listener: Subscription;
	constructor(
		private market: SupermarketService,
		private auth: AuthService,
		private messenger: SharedService,
		private fb: FormBuilder,
		private snackBar: SnackBarService,
		private cdr: ChangeDetectorRef
	) {
		this.newProductControl = this.fb.group({
			name: ['', Validators.required],
			price: ['', Validators.required],
			image: [''],
		});
	}
	ngOnInit() {
		this.listener = this.messenger
			.subscribe(({ type, payload }) => {
				if(type === SharedConstants.REFRESH_CATEGORIES)
					this.categorie$.subscribe();
			});
		this.categorie$.subscribe();
	}
	public get categorie$(): Observable<Category> {
		return this.market
			.getCategories()
			.pipe(
				map(res => res && res.data || []),
				tap(data => this.categories = data),
				tap(_ => this.cdr.detectChanges()),
			);
	}

	public isManager(): boolean {
		return this.auth.isManager();
	}
	public addTab(name: string): void {
		this.market.addCategory({ name } as Category)
			.subscribe(res => {
				if(!res) return;
				this.messenger.emit({ type: SharedConstants.REFRESH_CATEGORIES });
				this.snackBar.show(`The ${name} tab was created successfully.`);
			});
	}
	public removeTab(_id: string): void {
		this.market.deleteCategory(_id)
			.subscribe(res => {
				if(!res) return;
				this.messenger.emit({ type: SharedConstants.REFRESH_CATEGORIES });
				this.snackBar.show(`The tab was deleted successfully.`);
			});
	}
	public addProduct(newProduct, categoryID): void {
		this.market.addProduct(Object.assign(newProduct, { categoryID }))
			.subscribe(res => {
				if(!res) return;
				this.messenger.emit({
					type: SharedConstants.REFRESH_PRODUCT_LIST
				});
				this.snackBar.show(`${newProduct.name} added successfully.`);
			});
	}

	ngOnDestroy(): void {
		this.listener.unsubscribe();
	}
}
