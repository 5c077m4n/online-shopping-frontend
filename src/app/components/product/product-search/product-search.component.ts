import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { startWith, map } from 'rxjs/operators';

import { Product } from '../../../models/product';
import { SupermarketService } from '../../../services/supermarket.service';


@Component({
	selector: 'app-product-search',
	templateUrl: './product-search.component.html',
	styleUrls: ['./product-search.component.css']
})
export class ProductSearchComponent implements OnInit {
	private products: Product[] = [];
	public productControl = new FormControl();
	public filteredOption$: Observable<Product[]>;
	constructor(private market: SupermarketService) {}
	ngOnInit() {
		this.market.getProducts()
			.subscribe(res => this.products = res && res.data || []);
		this.filteredOption$ = this.productControl.valueChanges
			.pipe(
				startWith(''),
				map(item => (item)? this.filterNames(item) : this.products.slice()),
			);
	}

	public stopPropagation(e): void {
		e.stopPropagation();
	}

	private filterNames(value: string): Product[] {
		if(!this.products) return [];
		const filterValue = value.trim().toLowerCase();
		return this.products.filter(product =>
			product.name.toLowerCase().includes(filterValue)
		);
	}
}
