import {
	Component, OnInit, Input, OnDestroy, ChangeDetectionStrategy, ChangeDetectorRef
} from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { map, tap } from 'rxjs/operators';

import { SharedConstants } from '../../../services/shared/shared.enum';
import { Category } from '../../../models/category';
import { Product } from '../../../models/product';
import { SupermarketService } from '../../../services/supermarket.service';
import { AuthService } from '../../../services/auth.service';
import { SharedService } from '../../../services/shared/shared.service';


@Component({
	selector: 'app-product-list',
	changeDetection: ChangeDetectionStrategy.OnPush,
	templateUrl: './product-list.component.html',
	styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit, OnDestroy {
	@Input() public category: Category;
	public products: Product[];
	private listener: Subscription;
	constructor(
		private market: SupermarketService,
		private auth: AuthService,
		private messenger: SharedService,
		private cdr: ChangeDetectorRef
	) {}
	ngOnInit() {
		this.listener = this.messenger
			.subscribe(({ type, payload }) => {
				if(type === SharedConstants.REFRESH_PRODUCT_LIST)
					this.product$.subscribe();
			});
		this.product$.subscribe();
	}
	public get product$(): Observable<Product[]> {
		return this.market
			.getCategoryProducts(this.category._id)
			.pipe(
				map(res => res && res.data || []),
				map(data => this.products = data),
				tap(() => this.cdr.detectChanges()),
			);
	}

	public isManager(): boolean {
		return this.auth.isManager();
	}

	ngOnDestroy(): void {
		this.listener.unsubscribe();
	}
}
