import {
	Component, OnInit, Inject, OnDestroy, ChangeDetectionStrategy, ChangeDetectorRef
} from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import {
	MatBottomSheet, MatBottomSheetRef, MAT_BOTTOM_SHEET_DATA
} from '@angular/material';
import { Observable, Subscription } from 'rxjs';
import { map, tap, switchMap } from 'rxjs/operators';

import { Cart } from '../../models/cart';
import { CartItem } from '../../models/cart-item';
import { SharedConstants } from '../../services/shared/shared.enum';
import { SharedService } from '../../services/shared/shared.service';
import { SupermarketService } from '../../services/supermarket.service';
import { AuthService } from '../../services/auth.service';
import { LocalStorageService } from '../../services/local-storage.service';


@Component({
	selector: 'app-cart',
	changeDetection: ChangeDetectionStrategy.OnPush,
	templateUrl: './cart.component.html',
	styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit, OnDestroy {
	public cartControl: FormGroup;
	public countItems: number;
	private customerID: string;
	private listener: Subscription;
	constructor(
		private auth: AuthService,
		private market: SupermarketService,
		private messenger: SharedService,
		private storage: LocalStorageService,
		private fb: FormBuilder,
		private bottomSheet: MatBottomSheet,
		private cdr: ChangeDetectorRef,
	) {
		this.cartControl = this.fb.group({
			cartItems: this.fb.array([])
		});
	}
	ngOnInit() {
		this.customerID = this.auth.getCurrentUser()._id;
		this.listener = this.messenger
			.subscribe(({ type, payload }) => {
				if(type === SharedConstants.REFRESH_CART) {
					this.cart$.subscribe();
					this.countItem$.subscribe();
				}
			});
		this.cart$.subscribe();
		this.countItem$.subscribe();
	}

	public get cart$(): Observable<Cart> {
		return this.market.getCustomerCart(this.customerID)
			.pipe(
				map(res => res && res.data),
				tap(data => (data)? this.storage.add('cartID', data._id) : undefined),
				switchMap(data => (data)?
					this.market.getCartItems(data._id)
						.pipe(
							map(innerRes => innerRes && innerRes.data || []),
							tap(innerData => innerData.forEach(this.addCartItem, this)),
							tap(_ => this.cdr.detectChanges()),
						)
					: this.market.addCart({ customerID: this.customerID } as Cart)
				),
			);
	}
	public get countItem$(): Observable<number> {
		return this.market.countCartItems(this.storage.get('cartID'))
			.pipe(
				map(res => res && res.data),
				tap(data => this.countItems = data),
				tap(_ => this.cdr.detectChanges()),
			);
	}

	public get cartItems(): FormArray {
		return this.cartControl.get('cartItems') as FormArray;
	}
	public addCartItem(cartItem: CartItem): void {
		this.cartItems.push(this.fb.group({
			_id: [cartItem._id, Validators.required],
			name: ['', Validators.required],
			amount: [cartItem.amount, [Validators.required, Validators.pattern(/^\d+$/)]],
			cartID: [cartItem.cartID],
			productID: [cartItem.productID],
		}));
	}
	public removeItem(idx: number): void {
		this.cartItems.removeAt(idx);
	}

	public openBottomSheet(): void {
		this.bottomSheet.open(
			CartBottomSheetComponent,
			{ data: this.cartControl }
		);
	}
	ngOnDestroy(): void {
		this.listener.unsubscribe();
	}
}

@Component({ templateUrl: 'cart-bottom-sheet.component.html' })
export class CartBottomSheetComponent {
	constructor(
		private bottomSheetRef: MatBottomSheetRef<CartBottomSheetComponent>,
		private fb: FormBuilder,
		private market: SupermarketService,
		private storage: LocalStorageService,
		private messenger: SharedService,
		@Inject(MAT_BOTTOM_SHEET_DATA) public cartControl: FormGroup
	) {}

	public get cartItems(): FormArray {
		return this.cartControl.get('cartItems') as FormArray;
	}
	public changeAmount(idx: number, change: number): void {
		this.cartItems.value[idx].amount = change;
		this.market.addCartItem(this.cartItems.value[idx])
			.subscribe(res => {
				if(!res) return;
				this.messenger.emit({ type: SharedConstants.REFRESH_CART });
			});
	}
	public removeItem(idx: number): void {
		this.market.deleteCartItem(this.storage.get('cartID'), this.cartItems.value[idx]._id)
			.subscribe(res => {
				if(res.status !== 204) return;
				this.cartItems.removeAt(idx);
				this.messenger.emit({ type: SharedConstants.REFRESH_CART });
			});
	}

	public onSubmit(e: MouseEvent): void {
		e.preventDefault();
		this.bottomSheetRef.dismiss();
	}
}
