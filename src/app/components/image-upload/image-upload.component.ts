import { Component, OnInit } from '@angular/core';


@Component({
	selector: 'app-image-upload',
	templateUrl: './image-upload.component.html',
	styleUrls: ['./image-upload.component.css']
})
export class ImageUploadComponent implements OnInit {
	public selectedFile: File;
	constructor() {}
	ngOnInit() {}

	public onFileChange(event: any): void {
		this.selectedFile = event.target.files[0];
	}
}
