import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import {
	FormBuilder, FormGroup, Validators
} from '@angular/forms';
import { Router } from '@angular/router';

import { AuthService } from '../../services/auth.service';
import { SnackBarService } from '../../services/snack-bar.service';


@Component({
	selector: 'app-register',
	changeDetection: ChangeDetectionStrategy.OnPush,
	templateUrl: './register.component.html',
	styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
	public registerControl: FormGroup;
	constructor(
		private fb: FormBuilder,
		private router: Router,
		private auth: AuthService,
		private snackBar: SnackBarService
	) {
		this.registerControl = this.fb.group({
			username: ['', Validators.required],
			password: ['', Validators.required],
			password2: ['', Validators.required],
			email: ['', [Validators.email, Validators.required]],
			firstName: [''],
			lastName: [''],
			address: this.fb.group({
				city: ['', Validators.required],
				street: ['', Validators.required],
				buildingNumber: ['', [Validators.required, Validators.pattern(/^\d+$/)]],
				aptNumber: ['', [Validators.required, Validators.pattern(/^\d+$/)]],
			}),
			isManager: [false],
		}, { validator: this.matchingPasswords() });
	}
	ngOnInit() {}

	public onSubmit(): void {
		if(!this.registerControl.valid) return;
		this.auth.register(this.registerControl.value)
			.subscribe(data => {
				if(!data) return;
				this.snackBar.show(`Welcome!`);
				this.router.navigate(['']);
			});
	}

	private matchingPasswords(): Function {
		return (group: FormGroup): void => {
			const pwdIn = group.get('password');
			const pwd2In = group.get('password2');
			if(pwdIn.value !== pwd2In.value)
				pwd2In.setErrors({ notEquivalent: true });
		};
	}
}
